from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.core.window import Window
from kivy.uix.image import Image
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.network.urlrequest import UrlRequest

class FindTemperature(App):
    def build(self):
        self.window = GridLayout()
        self.window.cols = 1
        self.window.size_hint=(0.8,0.9)
        self.window.pos_hint={"center_x":0.5,"center_y":0.5}
        Window.size = (360,640)
        
        self.window.add_widget(Image(source="logo.png"))
        
        self.input_text = TextInput(
            size_hint=(1,0.2),
            font_size="20sp",
            padding_y="12sp",
            halign="center"
        )
        self.window.add_widget(self.input_text)

        self.btn = Button(
            text="GO!",
            size_hint=(1,0.2),
            bold=True,
            background_color="#0099ff"
        )
        self.window.add_widget(self.btn)
        self.btn.bind(on_press=self.find_temp)

        self.label = Label(
            text="Find a city",
            font_size="20sp",
            color="#007dd1"
        )
        self.window.add_widget(self.label)
        
        return self.window
    
    def find_temp(self, instance):
        def edit_label(request,result):
            temp = result["main"]["temp"]
            self.label.text =f"Today in {self.input_text.text} there are {temp} C°"
        link = f"https://api.openweathermap.org/data/2.5/weather?q={self.input_text.text}&appid=c9089416eb8dc131e2bfe8426a28ae41&units=metric"
        UrlRequest(link, edit_label)

FindTemperature().run()